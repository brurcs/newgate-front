const glob = require('glob');

module.exports = function ({ appendPaths = [], forPath = '/', home = true, index = true }) {
    const filePaths = glob
        .sync(forPath + '/*.md', { ignore: ['**/README.md'] })
        .map(f => `/${f.replace('.md', '')}`);

    let paths = [];

    if (forPath !== '/' && home) {
        paths.push(['/', '< Home']);
    }
    if (index) {
        paths.push(`/${forPath}/`);
    }
    paths = paths
        .concat(filePaths)
        .concat(appendPaths || []);
    return { [`/${forPath}/`]: paths };
}