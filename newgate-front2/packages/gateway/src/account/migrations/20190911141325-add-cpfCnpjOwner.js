/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Accounts', 'cpfCnpjOwner', {
    type: Sequelize.STRING,
    allowNull: false,
  }),

  down: (QI, Sequelize) => QI.removeColumn('Accounts', 'cpfCnpjOwner'),
};
