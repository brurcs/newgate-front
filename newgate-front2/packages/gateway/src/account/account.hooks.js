import updateIfExists from '../lib/hooks/updateIfExists';
import guestsQuery from './guestsQuery.hook';

const guestsQueryHook = guestsQuery();

/**
 * Account Hooks
 *
 * @name AccountHooks
 * @memberof module:Account
 */
export default {
  before: {
    find: [
      guestsQueryHook.beforeFind,
    ],
    create: [
      updateIfExists([
        'id',
      ], true),
      guestsQueryHook.beforeCreate,
    ],
    update: [
      guestsQueryHook.beforeUpdate,
    ],
    patch: [
      guestsQueryHook.beforePatch,
    ],
  },
  after: {
    find: [
      guestsQueryHook.afterFind,
    ],
  },
};
