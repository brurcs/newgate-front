import feathersSequelize from 'feathers-sequelize';
import AccountService from './account.service';
import AccountModel from './account.model';
import AccountHooks from './account.hooks';
import AccountSetup from '.';

jest.mock('feathers-sequelize');
jest.mock('./account.service');
jest.mock('./account.model');

describe('AccountSetup', () => {
  const hooks = jest.fn();
  const app = {
    service: jest.fn(() => ({ hooks })),
    use: jest.fn(),
  };

  it('enable service on /accounts', () => {
    const Model = {};
    const service = { Model };
    AccountModel.mockImplementation(() => Model);
    feathersSequelize.mockImplementation(({ model = Model }) => {
      expect(model).toBe(Model);
      return service;
    });
    AccountService.mockImplementation(({ model = Model }) => {
      expect(model).toBe(Model);
      return service;
    });
    AccountSetup(app);
    expect(app.use).toBeCalledWith('/accounts', service);
  });

  it('set hooks for service', () => {
    AccountSetup(app);
    expect(hooks).toBeCalledWith(AccountHooks);
  });
});
