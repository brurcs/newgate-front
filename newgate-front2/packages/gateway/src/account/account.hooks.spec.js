import updateIfExists from '../lib/hooks/updateIfExists';
import guestsQuery from './guestsQuery.hook';

jest.mock('../lib/hooks/updateIfExists', () => jest.fn(
  () => 'beforeCreate',
));
jest.mock('./guestsQuery.hook', () => jest.fn(() => ({
  beforeFind: 'beforeFind',
  beforeUpdate: 'beforeUpdate',
  afterFind: 'afterFind',
  beforePatch: 'beforePatch',
  beforeCreate: 'beforeCreate',
})));

/* eslint-disable global-require */
describe('account hooks', () => {
  it('register updateIfExists hook', () => {
    const hooks = require('./account.hooks').default;
    expect(updateIfExists).toBeCalledWith(['id'], true);
    expect(guestsQuery).toBeCalled();
    expect(hooks).toEqual({
      before: {
        find: [
          'beforeFind',
        ],
        create: [
          'beforeCreate',
          'beforeCreate',
        ],
        update: [
          'beforeUpdate',
        ],
        patch: [
          'beforePatch',
        ],
      },
      after: {
        find: [
          'afterFind',
        ],
      },
    });
  });
});
