import feathers from '@feathersjs/feathers';
import { NotAuthenticated } from '@feathersjs/errors';
import clientToken from './clientToken';

describe('clientToken', () => {
  let app;

  beforeEach(() => {
    app = feathers();
  });

  it('bypass services', () => {
    const service = {
      find: jest.fn(),
    };
    app.use('/foos', service);
    const context = {
      app,
      service: app.service('foos'),
      params: {},
    };
    const bypassServices = ['foos'];
    const hook = clientToken({ bypassServices });
    return hook(context);
  });

  it('bypass when skipAuth is set', () => {
    const service = {
      find: jest.fn(),
    };
    app.use('/foos', service);
    const context = {
      app,
      service: app.service('foos'),
      params: {
        skipAuth: true,
      },
    };
    const hook = clientToken();
    return hook(context);
  });

  it('throws if accessToken was not passed on query', async () => {
    expect.assertions(1);
    const hook = clientToken();

    // fake service
    const service = {
      find: jest.fn(),
    };
    app.use('/foos', service);

    const context = {
      app,
      service: app.service('foos'),
      params: {},
    };

    try {
      await hook(context);
    } catch (e) {
      expect(e).toBeInstanceOf(NotAuthenticated);
    }
  });

  it('throws if token was not found on database', async () => {
    expect.assertions(2);
    const hook = clientToken();

    // Fake tokens service
    const tokensService = {
      find: jest.fn(() => Promise.resolve([])),
    };
    app.use('/tokens', tokensService);

    // Fake service
    const service = {
      find: jest.fn(),
    };
    app.use('/foos', service);

    const context = {
      app,
      service: app.service('foos'),
      params: {
        query: {
          accessToken: 'not existent',
        },
      },
    };

    try {
      await hook(context);
    } catch (e) {
      expect(tokensService.find).toBeCalled();
      expect(e).toBeInstanceOf(NotAuthenticated);
    }
  });

  it('throws if token is expired', async () => {
    expect.assertions(2);
    const hook = clientToken();

    // Fake token
    const token = {
      accessToken: 'existent',
      createdAt: new Date('01/01/1900'),
      expiresIn: 9000,
    };

    // Fake tokens service
    const tokensService = {
      find: jest.fn(() => Promise.resolve([token])),
    };
    app.use('/tokens', tokensService);

    // Fake service
    const service = {
      find: jest.fn(() => Promise.resolve([token])),
    };
    app.use('/foos', service);

    const context = {
      app,
      service: app.service('foos'),
      params: {
        query: {
          accessToken: token.accessToken,
        },
      },
    };

    try {
      await hook(context);
    } catch (e) {
      expect(tokensService.find).toBeCalled();
      expect(e).toBeInstanceOf(NotAuthenticated);
    }
  });

  it('delete accessToken and keep going if token is valid', async () => {
    const hook = clientToken();

    // Fake token
    const token = {
      accessToken: 'existent',
      createdAt: new Date(),
      expiresIn: 9000,
    };

    // Fake tokens service
    const tokensService = {
      find: jest.fn(() => Promise.resolve([token])),
    };
    app.use('/tokens', tokensService);

    // Fake service
    const service = {
      find: jest.fn(() => Promise.resolve([token])),
    };
    app.use('/foos', service);

    const context = {
      app,
      service: app.service('foos'),
      params: {
        query: {
          accessToken: token.accessToken,
        },
      },
    };

    await hook(context);
    expect(tokensService.find).toBeCalledWith({
      query: { accessToken: token.accessToken },
      skipAuth: true,
    });
    expect(context.params.query.accessToken).toBeUndefined();
    expect(context.params.clientToken).toBe(token);
  });

  it('dont set clientToken if its already set', async () => {
    const token = {
      access_token: 'foo',
      expires_in: 9000,
      createdAt: new Date(),
    };
    app.clientToken = token;
    await clientToken(app);
    expect(app.clientToken).toBe(token);
  });
});
