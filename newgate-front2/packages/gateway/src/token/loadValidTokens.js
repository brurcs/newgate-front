/* eslint-disable camelcase */
import bcrypt from 'bcrypt';
import { Forbidden } from '@feathersjs/errors';
import { logger } from '@/logger';
import isTokenExpired from './isTokenExpired';

/**
 * Load only valid tokens from database
 *
 * @export
 * @memberof module:TokenHooks
 * @typedef module:TokenHooks.loadValidTokens
 * @name loadValidTokens
 * @param {Context} context
 * @returns {Promise.<Context>}
 */
export default async function loadValidTokens(context) {
  const { app, params, service } = context;
  if (params.skipAuth) return context;
  if (params.query && params.query.username && params.query.password) {
    const { username } = params.query;
    const users = await app.service('/users').find({
      query: {
        $or: [
          { email: username },
          { cpfCnpj: username },
          { phone: username },
        ],
      },
    });
    const checkPassword = user => user.password && bcrypt.compare(
      params.query.password,
      user.password,
    );
    if (users.length > 0) {
      // store user in context for persistOrsId hook use
      context.storedUser = users;

      if (await checkPassword(users[0])) {
        const tokens = await service.find(
          { skipAuth: true, query: { userId: users[0].id } },
        );
        const notExpired = token => !isTokenExpired(token);
        const validTokens = tokens.filter(notExpired);
        if (validTokens.length > 0) {
          logger.debug('found %d %s %s', validTokens.length, 'valid tokens found for', username);
          context.result = validTokens;
        } else if (tokens.length > 0) {
          logger.debug('found %d %s %s', tokens.length, 'expired tokens found for', username);
        } else {
          logger.debug('no tokens were found for %s', username);
        }
      } else {
        logger.debug('password invalid, no tokens for %s', username);
      }
    } else {
      logger.debug('no user found, no tokens for %s', username);
    }

    return context;
  }
  throw new Forbidden('username and/or password not received');
}
