/**
 * Token Hooks Module
 *
 * @module TokenHooks
 */

import allowMethods from '@/lib/hooks/allowMethods';
import loadValidTokens from './loadValidTokens';
import loadOrsIdToken from './loadOrsIdToken';
import loadOrsIdAccounts from './loadOrsIdAccounts';
import persistOrsId from './persistOrsId';
import setupClientToken from './clientToken';
import { ensureSkipAuth } from './skipAuth';

export const globalHooks = {
  before: {
    all: [
      ensureSkipAuth,
      setupClientToken({
        bypassServices: [
          'tokens',
          'users/passwordResets',
          'users/passwordChange',
          'hello',
        ],
      }),
    ],
  },
};

/**
 * hooks
 *
 * @name TokenHooks
 * @memberof module:Token
 * @property {module:TokenHooks.loadValidTokens} loadValidTokens
 * @property {module:TokenHooks.loadOrsIdToken} loadOrsIdToken
 * @property {module:TokenHooks.persistOrsId} persistOrsId
 */
export default {
  before: {
    all: [
      allowMethods('rest', ['find']),
      allowMethods('socketio', ['find']),
    ],
    find: [
      loadValidTokens,
      loadOrsIdToken,
      loadOrsIdAccounts,
      persistOrsId,
    ],
  },
};
