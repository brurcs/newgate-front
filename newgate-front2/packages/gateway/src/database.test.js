import app from './app';
import setupDatabase from './database';

describe('database', () => {
  it('sets a db with sequelize instance', () => {
    const dbCfg = app.get('database');
    setupDatabase(app);
    expect(app.db).toMatchObject({
      options: {
        host: process.env.MYSQL_HOST || dbCfg.host,
        dialect: dbCfg.dialect,
        hooks: {
          beforeDefine: expect.arrayContaining([
            expect.any(Function),
          ]),
        },
      },
    });
  });
});
