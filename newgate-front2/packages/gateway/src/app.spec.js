/* eslint-disable global-require */
import cors from 'cors';
import helmet from 'helmet';
import swagger from 'feathers-swagger';
import swaggerUi from 'swagger-ui-express';
import { json, urlencoded, rest } from '@feathersjs/express';
import socketio from '@feathersjs/socketio';
import configuration from '@feathersjs/configuration';
import setupJobs from './jobs';
import setupDatabase from './database';
import setupLogger from './logger';
import setupHello from './hello';
import setupMessage from './message';
import setupContato from './contato';
import setupUser from './user';
import setupToken from './token';
import setupAccount from './account';

jest.mock('cors');
jest.mock('helmet');
jest.mock('feathers-swagger');
jest.mock('swagger-ui-express', () => ({
  setup: jest.fn(),
}));
jest.mock('@feathersjs/socketio');
jest.mock('@feathersjs/configuration');

// mock feathers module
jest.mock('@feathersjs/feathers', () => () => (app) => {
  const config = require('../config/test.json');
  app.configure = fn => (typeof fn === 'function' ? fn(app) : null);
  app.use = jest.fn();
  app.get = jest.fn(param => config[param]);
  app.all = jest.fn();
  app.service = jest.fn(() => ({
    Model: {},
  }));
  app.db = {
    setupAssociations: jest.fn(),
  };

  return app;
});

// mock express module
jest.mock('@feathersjs/express', () => {
  const expressInstance = {};

  return {
    __esModule: true,
    default: cb => cb(expressInstance),
    json: jest.fn(),
    urlencoded: jest.fn(),
    rest: jest.fn(),
  };
});

jest.mock('./jobs');
jest.mock('./database');
jest.mock('./logger');
jest.mock('./hello');
jest.mock('./message');
jest.mock('./contato');
jest.mock('./user');
jest.mock('./token');
jest.mock('./account');

describe('app unit', () => {
  it('configure app', () => {
    require('./app');
    expect(configuration).toBeCalled();
    expect(helmet).toBeCalled();
    expect(swagger).toBeCalledWith(
      expect.objectContaining({
        specs: expect.any(Object),
      }),
    );
    expect(swaggerUi.setup).toBeCalledWith(
      null,
      expect.objectContaining({
        swaggerOptions: expect.objectContaining({
          url: 'http://localhost:3000/docs',
        }),
      }),
    );
    expect(json).toBeCalled();
    expect(urlencoded).toBeCalledWith(
      expect.objectContaining({ extended: true }),
    );
    expect(socketio).toBeCalled();
    expect(rest).toBeCalled();
    // expect(notFound).toBeCalled();
    expect(cors).toBeCalled();

    // domains
    expect(setupJobs).toBeCalled();
    expect(setupDatabase).toBeCalled();
    expect(setupLogger).toBeCalled();
    expect(setupHello).toBeCalled();
    expect(setupMessage).toBeCalled();
    expect(setupContato).toBeCalled();
    expect(setupUser).toBeCalled();
    expect(setupToken).toBeCalled();
    expect(setupAccount).toBeCalled();
  });
});
