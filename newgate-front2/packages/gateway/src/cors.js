import cors from 'cors';

export default function setupCors(app) {
  app.use(cors({
    origin: (origin, callback) => {
      const whitelist = [
        `http://${app.get('host')}:${app.get('port')}`,
        `https://${app.get('host')}:${app.get('port')}`,
        app.get('portalClienteURI'),
      ];
      if (!origin || whitelist.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        callback(new Error(`Origin ${origin} not allowed by CORS`));
      }
    },
  }));
}
