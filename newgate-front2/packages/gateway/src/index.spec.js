/* eslint-disable global-require */
import app from './app';
import { logger } from './logger';

jest.mock('./logger');
jest.mock('./app');

describe('index', () => {
  it('calls app listen on port 3000', () => {
    require('./index');
    expect(app.listen).toBeCalledWith('3000');
  });

  it('error on unhandledRejection', () => {
    const p = 'foo';
    const reason = 'bar';
    require('./index');
    process.emit('unhandledRejection', reason, p);
    expect(logger.error).toBeCalledWith(
      expect.any(String),
      p,
      reason,
    );
  });

  it('output welcome msg', (done) => {
    const oldVersion = process.env.APP_VERSION;

    require('./index');
    process.on('fakeServerListening', () => {
      expect(logger.info).toBeCalledWith(
        'Gateway %sapplication started on http://%s:%d',
        '',
        'localhost',
        '3000',
      );
      process.env.APP_VERSION = oldVersion;
      done();
    });
  });
});
