import setupHello from '.';

describe('hello', () => {
  it('show hello on root', (done) => {
    const send = jest.fn();
    const evaluate = () => {
      expect(send).toBeCalledWith(
        expect.any(String),
      );
      done();
    };
    const mock = {
      use: md => md({ path: '/' }, { send }, evaluate),
    };
    setupHello(mock);
  });

  it('skip for other urls', (done) => {
    const send = jest.fn();
    const evaluate = () => {
      expect(send).not.toBeCalled();
      done();
    };
    const mock = {
      use: md => md({ path: '/foo' }, { send }, evaluate),
    };
    setupHello(mock);
  });
});
