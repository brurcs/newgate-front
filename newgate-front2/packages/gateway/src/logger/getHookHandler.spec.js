import { inspect } from 'util';
import getHookHandler from './getHookHandler';
import logger from './winstonLogger';

jest.mock('./winstonLogger');

function createStringMatcher(...strings) {
  const finder = strings.map(s => `.*${s}`).join('');
  return new RegExp(finder);
}

describe('logger getHookHandler', () => {
  it('debugs with context data', () => {
    const context = {
      type: 'foo',
      path: 'bar',
      method: 'foobar',
    };
    const hookHandler = getHookHandler();
    hookHandler(context);
    expect(logger.debug).toBeCalledWith(
      expect.stringMatching(createStringMatcher(
        context.type,
        context.path,
        context.method,
      )),
    );
  });

  it.skip('debugs with stringified context', () => {
    const context = {
      type: 'foo',
      path: 'bar',
      method: 'foobar',
      toJSON: () => { },
    };
    const hookHandler = getHookHandler();
    hookHandler(context);
    expect(logger.debug).toHaveBeenLastCalledWith(
      expect.any(String),
      inspect(context),
    );
  });

  it('outputs context error', () => {
    const context = {
      error: 'foo',
    };
    const hookHandler = getHookHandler();
    hookHandler(context);
    expect(logger.error).toHaveBeenCalledWith('foo');
  });
});
