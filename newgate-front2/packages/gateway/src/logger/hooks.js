import getHookHandler from './getHookHandler';

// A hook that logs service method before, after and error
// See https://github.com/winstonjs/winston for documentation
// about the logger.
export default {
  before: {
    all: [getHookHandler()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [getHookHandler()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [getHookHandler()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
