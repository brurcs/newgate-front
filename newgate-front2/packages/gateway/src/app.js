import helmet from 'helmet';
import socketio from '@feathersjs/socketio';
import express, { json, urlencoded, rest } from '@feathersjs/express';
import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';

import setupHttpRequest from './httpRequest';
import setupCors from './cors';
import setupSwagger from './swagger';
import setupJobs from './jobs';
import setupDatabase from './database';

import setupLogger from './logger';
import setupHello from './hello';
import setupMobileRequest from './mobileRequest';
import setupFileRequest from './fileRequest';
import setupMessage from './message';
import setupContato from './contato';
import setupUser from './user';
import setupToken from './token';
import setupAccount from './account';

import { secureSkipAuth } from './token/skipAuth';

const app = express(feathers());
app.configure(configuration());

// Enable CORS, security, compression, favicon and body parsing
app.configure(setupCors);
app.use(helmet());
app.use(json());
app.use(urlencoded({ extended: true }));

// Set up Plugins and providers
app.configure(rest());

// Add global middlewares for REST
app.use(secureSkipAuth);

// add global middlewares for SOCKET
app.configure(socketio((io) => {
  io.use(secureSkipAuth);
}));


// FIXME: Find a way to test process.env.NODE_ENV
/* istanbul ignore next */
if (process.env.NODE_ENV !== 'production') {
  app.configure(setupSwagger);
}

// Setup jobs
app.configure(setupJobs);

// Setup database
app.configure(setupDatabase);

// Setup entities
app.configure(setupLogger);
// configure request helper
app.configure(setupHttpRequest);
app.configure(setupHello);
/* istanbul ignore next */
app.configure(setupToken);
app.configure(setupMobileRequest);
app.configure(setupFileRequest);
app.configure(setupMessage);
app.configure(setupContato);
app.configure(setupUser);
app.configure(setupAccount);

// Configure a middleware for 404s
// app.use(express.notFound());

// Setup model associations
app.db.setupAssociations();

export default app;
