/* istanbul ignore file */
import { logger } from '@/logger';
import PortalMobileError from '@/lib/PortalMobileError';

function genPMUpdator(app, params, data) {
  return async function updatePortalMobile(acordo) {
    const mobileRequest = app.service('mobileRequests');
    const url = '/portalcliente/v1/aceitenfdocemail';
    const method = 'get';
    const getParams = {
      email: data.email,
      acordo,
      numcpf: data.numcpf,
    };
    const ret = await mobileRequest.create({ url, params: getParams, method }, {
      clientToken: params.clientToken,
    });

    if (ret.data.status !== 89) {
      throw new PortalMobileError(ret.data);
    }
  };
}

export default function PasswordChangeService(app) {
  return {
    async create(data, params) {
      logger.debug('adding subscription for %s', data.numcpf);
      try {
        const userService = app.service('users');
        const updatePM = genPMUpdator(app, params, data);
        await updatePM(true);
        let retUser = await userService.get(data.userId);
        const subIdx = retUser.emailSubscriptions.findIndex(
          ({ numcpf }) => numcpf === data.numcpf,
        );
        if (subIdx < 0) {
          retUser = await userService.patch(retUser.id, {
            emailSubscriptions: [
              ...retUser.emailSubscriptions,
              { numcpf: data.numcpf, email: data.email },
            ],
          });
        }
        return retUser.emailSubscriptions;
      } catch (err) {
        logger.error(
          'error adding subscription for %s, details: %s',
          data.numcpf,
          err,
        );
        throw err;
      }
    },
    async remove(data, params) {
      logger.debug('removing subscription for %s', data.numcpf);
      try {
        const userService = app.service('users');
        const updatePM = genPMUpdator(app, params, data);
        await updatePM(false);
        let retUser = await userService.get(data.userId);
        const subIdx = retUser.emailSubscriptions.findIndex(
          ({ numcpf }) => numcpf === data.numcpf,
        );
        if (subIdx > -1) {
          const emailSubscriptions = [...retUser.emailSubscriptions];
          emailSubscriptions.splice(subIdx, 1);
          retUser = await userService.patch(retUser.id, { emailSubscriptions });
        }
        return retUser.emailSubscriptions;
      } catch (err) {
        logger.error(
          'error removing subscription for %s, details: %s',
          data.numcpf,
          err,
        );
        throw err;
      }
    },
  };
}
