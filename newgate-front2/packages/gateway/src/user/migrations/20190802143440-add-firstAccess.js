/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'firstAccess', {
    type: Sequelize.INTEGER,
    allowNull: false,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'firstAccess'),
};
