/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'login', {
    type: Sequelize.STRING,
  }),

  down: (QI, Sequelize) => QI.removeColumn('Users', 'login'),
};
