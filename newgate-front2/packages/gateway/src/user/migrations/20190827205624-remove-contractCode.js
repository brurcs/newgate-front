/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.removeColumn('Users', 'contractCode'),
  down: (QI, Sequelize) => QI.addColumn('Users', 'contractCode', {
    type: Sequelize.STRING,
  }),
};
