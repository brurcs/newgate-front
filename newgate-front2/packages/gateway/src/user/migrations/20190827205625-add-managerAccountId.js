/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'managerAccountId', {
    allowNull: true,
    type: Sequelize.INTEGER,
    references: {
      model: 'Accounts',
      key: 'id',
    },
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'managerAccountId'),
};
