/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'isAdmin', {
    type: Sequelize.BOOLEAN,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'isAdmin'),
};
