/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'name', {
    type: Sequelize.STRING,
    allowNull: false,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'name'),
};
