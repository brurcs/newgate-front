import feathersSequelize from 'feathers-sequelize';
import UserService from './user.service';
import PasswordResetService from './passwordReset.service';
import PasswordChangeService from './passwordChange.service';
import UserModel from './user.model';
import userHooks from './user.hooks';
import setupUser from '.';

jest.mock('feathers-sequelize');
jest.mock('./user.service');
jest.mock('./passwordReset.service');
jest.mock('./passwordChange.service');
jest.mock('./user.model');

describe('userSetup', () => {
  const hooks = jest.fn();
  const app = {
    service: jest.fn(() => ({ hooks })),
    use: jest.fn(),
  };

  it('enable service on /users', () => {
    const Model = {};
    const service = { Model };
    UserModel.mockImplementation(() => Model);
    feathersSequelize.mockImplementation(({ model = Model }) => {
      expect(model).toBe(Model);
      return service;
    });
    UserService.mockImplementation(({ model = Model }) => {
      expect(model).toBe(Model);
      return service;
    });
    setupUser(app);
    expect(app.use).toBeCalledWith('/users', service);
  });

  it('enable passwordResets service on /users/passwordResets', () => {
    const service = {};
    PasswordResetService.mockImplementation(() => service);
    setupUser(app);
    expect(app.use).toBeCalledWith('/users/passwordResets', service);
  });

  it('enable passwordResets service on /users/passwordChanges', () => {
    const service = {};
    PasswordChangeService.mockImplementation(() => service);
    setupUser(app);
    expect(app.use).toBeCalledWith('/users/passwordChanges', service);
  });

  it('set hooks for service', () => {
    setupUser(app);
    expect(hooks).toBeCalledWith(userHooks);
  });
});
