describe('OrsegupsID e2e', () => {
  // FIXME: Enable staging orsegups ID cors for localhost, incuca/clientes/orsegups#20
  it.skip('Calls orsegupsID root', () => {
    const url = Cypress.env('VUE_APP_ORSEGUPSID_URI');
    cy.server();
    cy.route(url).as('getID');
    cy.visit('/about');
    cy.wait('@getID')
      .its('response.body')
      .should('have.property', '_links');
  });
});
