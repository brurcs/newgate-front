import { getStore } from '~/tests/utils';
import dashboard from '@/store/views/dashboard';

describe('dashboard view store', () => {
  it('exports a module', () => {
    expect(dashboard).toMatchObject({
      namespaced: true,
    });
  });

  it('check if its first user login and show dialog', async () => {
    const user = {
      id: 1,
      firstAccess: true,
    };
    const users = {
      namespaced: true,
      state: {
        copy: user,
      },
      actions: {
        get: jest.fn(() => Promise.resolve()),
      },
    };
    const session = {
      namespaced: true,
      state: {
        token: {
          userId: user.id,
        },
      },
    };
    const store = getStore({ dashboard, users, session });
    await store.dispatch('dashboard/checkUserFirstLogin');
    expect(store.state.dashboard.firstAccess).toBeTruthy();
    expect(store.getters['dashboard/currentUserId']).toBe(1);
    expect(store.getters['dashboard/isPasswordDialogOpen']).toBeTruthy();
  });

  it('do not show dialog if user already accessed before', async () => {
    const user = {
      id: 1,
      firstAccess: false,
    };
    const users = {
      namespaced: true,
      state: {
        copy: user,
      },
      actions: {
        get: jest.fn(() => Promise.resolve()),
      },
    };
    const session = {
      namespaced: true,
      state: {
        token: {
          userId: user.id,
        },
      },
    };
    const store = getStore({ dashboard, users, session });
    await store.dispatch('dashboard/checkUserFirstLogin');
    expect(store.state.dashboard.firstAccess).toBeFalsy();
    expect(store.getters['dashboard/currentUserId']).toBe(1);
    expect(store.getters['dashboard/isPasswordDialogOpen']).toBeFalsy();
  });

  it('change firstAccess when user changes password', async () => {
    const store = getStore({ dashboard });
    await store.dispatch('dashboard/changeUserPassword');
    expect(store.firstAccess).toBeFalsy();
  });
});
