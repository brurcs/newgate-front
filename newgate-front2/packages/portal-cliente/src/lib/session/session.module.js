import localforage from 'localforage';
import LogRocket from 'logrocket';

/**
 * Session Store
 * @namespace store.session
 * @memberof store
 */
export default {
  namespaced: true,
  /**
   * @namespace state
   * @memberof store.session
   */
  state: {
    /**
     * Session Token
     * @memberof store.session.state
     * @type Token
     */
    token: null,
    /**
     * Session Accounts
     * @memberof store.session.state
     * @type Array<Account>
     */
    userAccounts: [],
    /**
     * Session Selected Account
     * @memberof store.session.state
     * @type Account
     */
    currentAccount: null,
    /**
     * Session User
     * @memberof store.session.state
     * @type User
     */
    currentUser: null,
    /**
     * These functions will be triggered on account change
     * @memberof store.session.state
     * @type Array<Function>
     */
    accountChangeListeners: [],
  },
  /**
   * @namespace getters
   * @memberof store.session
   */
  getters: {
    /**
     * Check if session token is expired
     *
     * @returns {Boolean} true if it's expired
     */
    isTokenExpired(state) {
      const { token } = state;
      if (token && token.expiresIn && token.createdAt) {
        const { expiresIn, createdAt } = token;
        const expireDate = new Date(createdAt.valueOf());
        expireDate.setMilliseconds(
          expireDate.getMilliseconds() + expiresIn * 1000,
        );
        return expireDate <= new Date();
      }
      return true;
    },
    /**
     * Check if session currentAccount is up to date
     *
     * @returns {Boolean} true if it's valid
     */
    isAccountValid(state) {
      return (givenAccounts) => {
        const { currentAccount } = state;
        if (!currentAccount) return false;
        return givenAccounts.some(
          acc => JSON.stringify(acc) === JSON.stringify(currentAccount),
        );
      };
    },
    /**
     * Check if user is logged in (has a valid session)
     *
     * @returns {Boolean} true if it's logged in and token is not expired
     */
    loggedIn(state, getters) {
      return state.token && !getters.isTokenExpired;
    },
    /**
   * Check if user is logged in (has a valid session)
   *
   * @returns {Array<Account>} list of current user accounts
   */
    accounts(st) {
      return st.userAccounts;
    },
    guestFeatures({ currentAccount, currentUser }) {
      return (guestId = currentUser.id) => {
        if (currentUser && currentAccount && currentAccount.guests) {
          const guest = currentAccount
            .guests
            .find(g => g.id === guestId);
          return guest ? guest.features : [];
        }
        return [];
      };
    },
  },
  /**
   * @namespace mutations
   * @memberof store.session
   */
  mutations: {
    /**
     * Set session token
     *
     * @param {Token} token
     */
    setToken(state, token) {
      state.token = token;
    },
    /**
     * Set session accounts
     *
     * @param {Array<Account>} accounts
     */
    setUserAccounts(st, accounts) {
      st.userAccounts = accounts;
    },
    /**
     * Set session selected account
     *
     * @param {Account} account
     */
    setCurrentAccount(st, currentAccount) {
      st.currentAccount = currentAccount;

      // Trigger account listeners
      st.accountChangeListeners.forEach(
        listener => listener(),
      );
    },
    /**
     * Set session current user
     *
     * @param {User} currentUser
     */
    setCurrentUser(st, currentUser) {
      st.currentUser = currentUser;
    },
    /**
     * Add account change listener
     *
     * @param {Function} listener will be called when current account changes
     */
    addAccountChangeListener(st, listener) {
      st.accountChangeListeners.push(listener);
    },
    /**
     * Remove account change listener
     *
     * @param {Function} listener the listener to remove
     */
    removeAccountChangeListener(st, listener) {
      st.accountChangeListeners = st.accountChangeListeners.filter(
        item => item !== listener,
      );
    },
    clearAccountChangeListeners(st) {
      st.accountChangeListeners = [];
    },
  },
  /**
   * @namespace actions
   * @memberof store.session
   */
  actions: {
    /**
     * Creates a new session for a given token
     *
     * @memberof store.session.actions
     * @param {Object} payload
     * @param {Token} payload.token
     */
    async create({ commit }, { token }) {
      await localforage.setItem('sessionToken', token);
      commit('setToken', token);
    },
    /**
     * load token and account from localStorage or indexDB (depends on browser)
     * if it exists
     *
     * @memberof store.session.actions
     */
    async loadFromStorage({ commit }) {
      const token = await localforage.getItem('sessionToken');
      if (token) {
        commit('setToken', token);
      }
      const account = await localforage.getItem('sessionAccount');
      if (account) {
        commit('setCurrentAccount', account);
      }
    },
    /**
     * Removes current active session
     *
     * @memberof store.session.actions
     */
    async remove({ commit }) {
      // clear listeners to not trigger events on leaving
      commit('clearAccountChangeListeners');

      // clear session storage
      await localforage.removeItem('sessionToken');
      commit('setToken', null);
      await localforage.removeItem('sessionAccount');
      commit('setCurrentAccount', null);
    },
    /**
     * Load current active user
     *
     * @memberof store.session.actions
     */
    async loadUser({ dispatch, commit, state }) {
      const { token } = state;
      const currentUser = await dispatch('users/get', token.userId, { root: true });
      if (process.env.VUE_APP_LOGROCKET) {
        LogRocket.identify(currentUser.id, {
          name: currentUser.name,
          email: currentUser.email,
          // TODO: add managerId to LogRocket
          // managerId: currentAccount.userOnwerId
        });
      }
      commit('setCurrentUser', currentUser);
    },
    /**
     * Load current active user accounts
     *
     * @memberof store.session.actions
     */
    async loadUserAccounts({
      dispatch, rootState, commit, getters,
    }) {
      const user = rootState.session.currentUser;
      // get manager accounts or guest accounts
      const mineAccounts = await dispatch('accounts/find', { query: { userOwnerId: user.id } }, { root: true });
      const otherAccounts = await dispatch('accounts/find', { query: { guestId: user.id } }, { root: true });
      const accounts = [...mineAccounts, ...otherAccounts];
      if (accounts.length < 1) {
        throw Error('Não foi possível encontrar contas válidas para o usuário informado');
      }
      commit('setUserAccounts', accounts);
      // if there is no a local storage account, or is outdated
      if (!getters.isAccountValid(accounts)) {
        await dispatch('updateCurrentAccount', accounts[0]);
      }
    },
    /**
     * Update store and session account
     *
     * @memberof store.session.actions
     */
    async updateCurrentAccount({ commit }, account) {
      await localforage.setItem('sessionAccount', account);
      commit('setCurrentAccount', account);
    },
  },
};
