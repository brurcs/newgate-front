export default function joinFields(fields, { from, char = ' - ', transform } = {}) {
  const fieldKeys = Array.isArray(fields) ? fields : Object.keys(fields);
  return fieldKeys
    .filter(f => (from && from[f]) || fields[f])
    .map(f => (from && from[f]) || fields[f])
    .map(f => (transform ? transform(f) : f))
    .join(char);
}
