import dashboard from './dashboard';
import profile from './profile';
import guestsList from './guestsList';
import contactsList from './contactsList';
import alarmsList from './alarmsList';
import invoices from './invoices';
import documents from './documents';
import notifications from './notifications';

export default {
  namespaced: true,
  modules: {
    dashboard,
    profile,
    guestsList,
    contactsList,
    alarmsList,
    invoices,
    documents,
    notifications,
  },
};
